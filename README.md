## GUIA CREACION APIS REST EN PYTHON
Proyecto que contiene un par de ejemplos para crear servicios REST en python.

### Basado en la guia : https://realpython.com/flask-connexion-rest-api/#what-rest-is
puede verse la guia original para entender bien a fondo el concepto.

### Prerequisites
Actualizar la version de pip:

```
python -m pip install --upgrade pip
```

Intalar modulo de ambiente virutal

```
pip install virtualenv
```

Crear un ambiente virtual

```
python3 -m venv env
```

Instalar librerías de flask

```
pip install flask
```

Instalar librerías de connexion

```
pip install connexion[swagger-ui] 
```


### Ejecutar

ejecutar el ambiente virtual

* **linux** - *source env/bin/activate*
* **powershell(windows)** - *.\env\Scripts\Activate.ps1*


ejecutar la prueba del sitio

```
python .\Server.py
```

* **en el navegador:** - *http://127.0.0.1:5000*

ejecutar la prueba del api

```
python .\ServerRest.py
```

* **en el navegador:** - *http://127.0.0.1:5000/api/people*
* **en otro navegador:** - *http://127.0.0.1:5000/api/ui*